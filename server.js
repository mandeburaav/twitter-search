const express = require("express");
const cors = require('cors')
const bodyParser = require("body-parser");
const path = require("path");
const tweets = require("./routes/api/tweets");

const app = express();

app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use("/", tweets);

if (process.env.NODE_ENV === "production") {
  
  app.use(express.static("client/build"));

  // app.get("/search", (req, res) => {
  //   res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
  // });
}

const port = process.env.PORT || 3005;

app.listen(port, () => console.log(`Server is listening on ${port}`));