const express = require("express");
const router = express.Router();
const twitterClient = require("../../twitter");
 
var client = twitterClient;
 

router.get('/search', (req, res, next) => {
  const query = req.query.q;
  client.get('search/tweets', {q: query})    
    .then(data => {
      const resData = data.statuses.map(item => {
        return {
          id: item.id,
          user_img: item.user.profile_image_url,
          user_name: item.user.name,
          created_at: item.created_at,
          text: item.text
        };
      });
      res.json(resData);
    })
    .catch(err => {
      return res.status(404).json(err);
    });
})


module.exports = router;