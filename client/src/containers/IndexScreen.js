import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actionCreators from '../modules/actions';
import SearchBar from './SearchBar'; 
import TweetBody from '../components/TweetBody'; 
import Header from '../components/Header';
import SearchResult from './SearchResult';


class IndexScreen extends Component {
  componentDidMount() {
    this.props.loadTweets('%23javascript')
  }

  render() {
    return (
        <div>
            <Header />
            <SearchResult handleClick={this.props.loadTweets} />
        </div>
      
    );
  }
}

const mapStateToProps = (state) => {
  return state
};

export default connect(mapStateToProps, actionCreators)(IndexScreen);
