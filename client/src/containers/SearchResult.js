import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actionCreators from '../modules/actions';
import TweetBody from '../components/TweetBody'; 
import Typography from '@material-ui/core/Typography';
import SearchBar from './SearchBar'; 
import queryString from 'query-string';


class SearchResult extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
        <div>
            <SearchBar  />
            <Typography variant="h5" color="textSecondary" component="p">
                Your search results:   
            </Typography>
            <TweetBody tweets={this.props.tweets}/>
        </div>
      
    );
  }
}

const mapStateToProps = (state) => {
  return state
};

export default connect(mapStateToProps, actionCreators)(SearchResult);