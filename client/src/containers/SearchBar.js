import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actionCreators from '../modules/actions';
import history from '../utils/history';

import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from '@material-ui/icons/Search';
import './containers.css'; 


class SearchBar extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      value: ''    };
    
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  

  onChange(e) {
    e.preventDefault();
    this.setState({ value: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();
    const queryData = this.state.value;
    this.state.oldQuery = queryData;
    this.props.loadTweets(this.state.oldQuery);
    history.push(`/search?q=${queryData}`);
  }

  render() {
    return (
        <form onSubmit={this.onSubmit}>
          <Paper className="tweets-container">
                <InputBase
                    className="tweet-input"
                    type = "text"
                    name = "search"
                    onChange={this.onChange}
                    placeholder = "Search "
                    value={this.state.value}
                />
                <IconButton className="tweets-icon" aria-label="Search" type="submit" onClick={this.onSubmit}>
                    <SearchIcon />
                </IconButton>
          </Paper>
        </form>
    );
  }
}

const mapStateToProps = (state) => {
  return state
};

export default connect(mapStateToProps, actionCreators)(SearchBar);