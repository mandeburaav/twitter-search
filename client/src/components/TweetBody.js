import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(theme => ({
    card: {
      maxWidth: 500,
      margin: '20px auto',
    },
    media: {
      height: 0,
      paddingTop: '56.25%', // 16:9
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
}));


const TweetBody = (props) => {

    const classes = useStyles();
    const query = props.tweets;
    return (
        <div>
            { query.map(tweet =>
                <Card className={classes.card} key={tweet.id}>
                    <CardHeader
                        avatar={
                            <Avatar aria-label="Recipe" className={classes.avatar} src={tweet.user_img}></Avatar>
                        }
                        title={tweet.user_name}
                        subheader= {tweet.created_at}
                    />
                    <CardContent>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {tweet.text}    
                        </Typography>
                    </CardContent>
                </Card>
            )}
        </div>      
    );
}

export default TweetBody;