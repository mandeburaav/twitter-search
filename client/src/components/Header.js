import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
  heading: {
    color: '#03a9f4'
  }
});


export default function Heading() {
    const classes = useStyles();


    return (
        <Typography  className="mui--text-display2"  variant="h4" className={classes.heading} >Wanna search some tweets?</Typography >
    );
}
