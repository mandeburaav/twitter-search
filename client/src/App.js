import React, { Component } from 'react';
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import reducer from "./modules/reducer";
import IndexScreen from './containers/IndexScreen';
import SearchResult from './containers/SearchResult';
import history from './utils/history';

import Container from '@material-ui/core/Container';
import './App.css';


const store = createStore(
  reducer,
  compose(
    applyMiddleware(thunk),
    window.devToolsExtension ? window.devToolsExtension() : f => f
  )
);

class App extends Component {
  constructor() {
    super();
    this.state = {
      data: []
    }
  }

  render() {
    return (
      <Provider store={store}>
        <Router history={history}>
          <Container className="App" maxWidth="sm">
            <Route exact path="/" component={IndexScreen} />
            <Route path="/search" component={SearchResult} />
          </Container>
        </Router>
      </Provider>
    );
  }
}

export default App;
