import {
    FETCH_TWEETS_BEGIN,
    FETCH_TWEETS_SUCCESS
    // FETCH_TWEETS_FAILURE
} from './actionsTypes';

const initialState = {
    query: '',
    tweets: [],
    loading: false,
    error: null,
    tweet: ''
};

export default function tweetsReducer(state = initialState, action) {
    switch (action.type) {
      case FETCH_TWEETS_BEGIN:
        return {
          ...state,
          query: action.query
        };
        
      case FETCH_TWEETS_SUCCESS:
        return {
          ...state,
          tweets: action.tweet
        };

      // case FETCH_TWEETS_FAILURE:
      //   return {
      //     ...state,
      //     loading: false,
      //     error: action.payload.error,
      //     items: []
      //   };
      default:
        return state;
    }
}
