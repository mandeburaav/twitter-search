import axios from "axios";

import {
    FETCH_TWEETS_BEGIN,
    FETCH_TWEETS_SUCCESS,
} from './actionsTypes';

export const setQuery = queryData =>{
  return{
    type: FETCH_TWEETS_BEGIN,
    query: queryData
  }
}

export const loadTweets = (queryData) => dispatch => {
  dispatch(setQuery(queryData));
  axios
    .get("http://localhost:3005/search?q=" + queryData)
    .then((response) => {
        dispatch(setTweets(response.data))
    })
};

export function setTweets(data){
  return{
    type: FETCH_TWEETS_SUCCESS,
    tweet: data
  }
}

